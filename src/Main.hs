module Main where

import Debug.Trace
import qualified Data.ByteString               as B
import qualified Data.ByteString.Internal      as BI
import qualified Data.ByteString.Lazy          as BL
import qualified Data.ByteString.Lazy.Internal as BLI
import qualified Data.ByteString.Char8         as C
import Data.Binary.Get
import Data.Binary.Put
import Data.Digest.Pure.MD5
import qualified Data.Map                      as M
import System.Directory
import System.Environment
import System.FilePath
import System.IO

tocEntrySize = 296

-- | (Offset (only used in reading), Size, Bytes)
type FileEntry = (Int, Int, B.ByteString)

data PakState
  = PakState { rootPath :: FilePath
             , fileMap :: M.Map FilePath FileEntry
             }

newPak fp = PakState { rootPath = fp, fileMap = M.fromList [] }

addFileEntry fp foffs fsize fbytes pak = pak { fileMap = M.insert fp (foffs, fsize, fbytes) (fileMap pak) }

normalizePath path pak = drop ((length $ rootPath pak) + 1) (map repl path)
  where
    repl '\\' = '/'
    repl c    = c

processFile :: FilePath -> PakState -> IO PakState
processFile fp pak = do
  fh <- openFile fp ReadMode
  fsize <- hFileSize fh >>= pure . fromIntegral
  fbytes <- B.hGet fh fsize
  putStrLn ("Adding file " ++ fp ++ " -> " ++ (normalizePath fp pak))
  pure $ addFileEntry (normalizePath fp pak) 0 fsize fbytes pak

processDirEntries :: FilePath -> [FilePath] -> PakState -> IO PakState
processDirEntries dp [] pak = pure pak
processDirEntries dp (fp : fps) pak = do
  let abspath = dp ++ [pathSeparator] ++ fp
  isdir <- doesDirectoryExist abspath
  pak' <- if isdir then processDir abspath pak else processFile abspath pak
  processDirEntries dp fps pak'

processDir :: FilePath -> PakState -> IO PakState
processDir fp pak = do
  ents <- listDirectory fp
  processDirEntries fp ents pak

putFilesToc :: [(FilePath, FileEntry)] -> Int -> Put
putFilesToc [] _ = pure ()
putFilesToc ((fp, (_, fsize, fbytes)) : xs) offs = do
  let fpad = replicate (256 - length fp) '\0'
  putByteString $ C.pack $ fp                -- write filename
  putByteString $ C.pack $ fpad              -- pad to filename 256 bytes
  putByteString $ C.pack $ show (md5 $ BL.fromStrict fbytes) -- write file md5 checksum
  putWord32le $ fromIntegral offs            -- write file offset
  putWord32le $ fromIntegral fsize           -- write file size
  putFilesToc xs (offs + fsize)

putFilesContent :: [(FilePath, FileEntry)] -> Put
putFilesContent [] = pure ()
putFilesContent ((fp, (_, fsize, fbytes)) : xs) = do
  putByteString $ fbytes
  putFilesContent xs

putPakFile :: PakState -> Put
putPakFile pak = do
  let tocSize = ((M.size $ fileMap pak) * tocEntrySize)
  let files = (M.toList $ fileMap pak)
  putByteString $ C.pack "VMPACK"     -- id
  putWord32le 0                       -- version
  putWord32le $ fromIntegral tocSize  -- toc size in bytes
  putFilesToc files (tocSize + 14)
  putFilesContent files

getPakFilesToc :: Int -> PakState -> Get PakState
getPakFilesToc 0 pak = pure pak
getPakFilesToc numfiles pak = do
  fpath <- getByteString 256 >>= pure . removeNull . C.unpack
  checksum <- getByteString 32 >>= pure . C.unpack
  foffs <- getWord32le >>= pure . fromIntegral
  fsize <- getWord32le >>= pure . fromIntegral
  getPakFilesToc (numfiles - 1) (addFileEntry (trace fpath fpath) foffs fsize B.empty pak)
  where
    removeNull = filter (\c -> c /= '\0')

getPakFileContent :: Int -> Int -> Get B.ByteString
getPakFileContent foffs fsize = do
  skip foffs
  getByteString fsize

getPakFilesContent :: [(FilePath, FileEntry)] -> PakState -> Get PakState
getPakFilesContent [] pak = pure pak
getPakFilesContent ((fp, (foffs, fsize, _)) : xs) pak = do
  bread <- bytesRead >>= pure . fromIntegral
  fbytes <- lookAhead $ getPakFileContent (foffs - bread) fsize
  getPakFilesContent xs (addFileEntry fp foffs fsize fbytes pak)

getPakFile :: FilePath -> Get (Either String PakState)
getPakFile fp = do
  pakid <- getByteString 6 >>= pure . C.unpack
  if pakid == "VMPACK"
    then do
      version <- getWord32le >>= pure . fromIntegral
      tocsize <- getWord32le >>= pure . fromIntegral
      let numfiles = tocsize `div` tocEntrySize
      pak <- getPakFilesToc numfiles $ newPak fp
      getPakFilesContent (M.toList $ fileMap pak) pak >>= pure . Right
    else do
      pure $ Left "Not a VMPACK file"

pack dirname outfile = do
  pak <- processDir dirname $ newPak dirname
  fh <- openFile outfile WriteMode
  BL.hPut fh $ runPut (putPakFile pak)
  hClose fh
  putStrLn ("Wrote " ++ (show $ M.size $ fileMap pak) ++ " files to " ++ outfile ++ ".")

unpack pakfile outdir = do
  fh <- openFile pakfile ReadMode
  fsize <- hFileSize fh >>= pure . fromIntegral
  contents <- BL.hGet fh fsize
  case runGet (getPakFile outdir) contents of
    (Right pak) -> do
      --extractPakFile pak
      print $ fileMap pak
    (Left err) -> do
      putStrLn ("Error: " ++ err)

printUsage =
  putStrLn "Usage:\n\tCreate a pak file: vmpak pack {directory} {outfile}\n\tExtract a pak file: vmpak unpack {pakfile} {outdir}"

main :: IO ()
main = do
  args <- getArgs
  case args of
    ["pack", dirname, outfile] -> pack dirname outfile
    ["unpack", pakfile, outdir] -> unpack pakfile outdir
    ["test", dirname, outfile] -> do
      pack dirname outfile
      unpack outfile (dirname ++ "_unpack_test")
    _ -> printUsage
